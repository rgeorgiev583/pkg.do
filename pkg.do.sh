#!/bin/bash

export CONF_FILENAME=/etc/pkg.do.conf

export SRC_PATH=./lib            #/usr/lib/pkg
export LIB_PATH=/tmp/pkg/lib     #/var/lib/pkg
export CACHE_PATH=/tmp/pkg/cache #/var/cache/pkg
export TARGET_PATH=/tmp/pkg/root #/
export SOURCE_PREFIX=/tmp/pkg/repo

# shellcheck source=/dev/null
source "${CONF_FILENAME}" 2>/dev/null

action=$1
shift 1

function do_action() {
    if [[ $# -eq 0 ]]; then
        if [[ ${action} != sync ]]; then
            echo 'error: no packages specified' >&2
            return 2
        fi

        redo-ifchange "${LIB_PATH}/sync"
        return 0
    fi

    local -a targets
    for package_name; do
        targets+=("${LIB_PATH}/${package_name}.${action}")
    done
    redo-ifchange "${targets[@]}"
}

function init() {
    mkdir -p "${LIB_PATH}/defs" "${LIB_PATH}/installed"
    ln -s "$(realpath "${SRC_PATH}")"/default.*.do "$(realpath "${SRC_PATH}")/sync.do" "$(realpath "${SRC_PATH}")/upgrade.do" "$(realpath "${LIB_PATH}"/installed)"
}

function update() {
    rsync -az "${SOURCE_PREFIX}/lib/defs/" "${LIB_PATH}/defs"
}

function upgrade() {
    if [[ $# -gt 0 ]]; then
        echo 'error: partial upgrades are not supported' >&2
        return 3
    fi

    redo-ifchange "${SRC_PATH}/upgrade"
}

case ${action} in
init)
    init || exit $?
    ;;

update)
    update || exit $?
    ;;

upgrade)
    upgrade || exit $?
    ;;

sync | sync-nodeps | install | uninstall | uninstall-cascade)
    do_action "$@" || exit $?
    ;;

*)
    echo 'error: invalid action specified' >&2
    exit 1
    ;;

esac
