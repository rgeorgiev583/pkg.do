#!/bin/bash

name=${1%.*}
if ! source "${LIB_PATH}/defs/${name}"; then
    echo "error: package ${name} does not exist"
    exit 1
fi

for dependency in "${dependencies[@]}"; do
    targets+=("${dependency}.sync")
done

redo "${targets[@]}" "${name}.sync-nodeps"
