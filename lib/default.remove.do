#!/bin/bash

name=${1%.*}
if ! source "${LIB_PATH}/installed/${name}"; then
    echo "error: package ${name} is not installed"
    exit 1
fi

for filename in "${files[@]}"; do
    if [[ -d ${filename} ]]; then
        directories+=("${filename}")
    else
        nondirectories+=("${filename}")
    fi
done

rm -f "${nondirectories[@]}"
rm -df "${directories[@]}" 2>/dev/null || true
