#!/bin/bash

name=${1%.*}

rsync -a "${CACHE_PATH}/${name}/" "${TARGET_PATH}"
