#!/bin/bash

shopt -s nullglob

for package_filename in "${LIB_PATH}"/installed/*; do
    package_name=$(basename "${package_filename}")
    targets+=("${package_name}.install")
done

if [[ ${#targets[@]} -eq 0 ]]; then
    echo "nothing to upgrade"
    exit
fi

redo-ifchange "${targets[@]}"
