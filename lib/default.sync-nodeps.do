#!/bin/bash

name=${1%.*}
if ! source "${LIB_PATH}/defs/${name}"; then
    echo "error: package ${name} does not exist"
    exit 1
fi

mkdir -p "${CACHE_PATH}/${name}" || exit 1
rsync -az --delete "${SOURCE_PREFIX}/cache/${name}/${version}/" "${CACHE_PATH}/${name}"
