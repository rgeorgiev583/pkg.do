#!/bin/bash

name=${1%.*}
if ! source "${LIB_PATH}/defs/${name}"; then
    echo "error: package ${name} does not exist"
    exit 1
fi
if ! source "${LIB_PATH}/installed/${name}"; then
    echo "error: package ${name} is not installed"
    exit 1
fi

for dependency in "${dependencies[@]}"; do
    targets+=("${dependency}.uninstall-cascade")
done

redo "${targets[@]}" "${name}.remove"

rm "${LIB_PATH}/installed/${name}"
