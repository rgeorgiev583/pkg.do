#!/bin/bash

shopt -s nullglob

name=${1%.*}
source "${LIB_PATH}/defs/${name}" || exit 1

source "${LIB_PATH}"/defs/*.rel

dependants_arrname=${name//-/_}_dependants[@]
dependants=${!dependants_arrname}

for conflict in "${dependants[@]}"; do
    if [[ -f "${LIB_PATH}/installed/${conflict}" ]]; then
        installed_dependants+=("${conflict}")
    fi
done

if [[ ${#installed_dependants} -gt 0 ]]; then
    echo "error: uninstallation of ${name} would break the following packages which depend on it: ${installed_dependants[*]}" >&2
    exit 2
fi

redo "${name}.remove"

rm -f "${LIB_PATH}/installed/${name}"
