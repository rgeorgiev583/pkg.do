#!/bin/bash

shopt -s nullglob

for package_filename in "${LIB_PATH}"/installed/*; do
    package_name=$(basename "${package_filename}")
    targets+=("${package_name}.sync")
done

if [[ ${#targets[@]} -eq 0 ]]; then
    echo "nothing to sync"
    exit
fi

redo-ifchange "${targets[@]}"
