#!/bin/bash

shopt -s nullglob

name=${1%.*}

redo-stamp <"${LIB_PATH}/defs/${name}"

if ! source "${LIB_PATH}/defs/${name}"; then
    echo "error: package ${name} does not exist"
    exit 1
fi

source "${LIB_PATH}"/defs/*.rel

conflicts_arrname=${name//-/_}_conflicts[@]
conflicts=${!conflicts_arrname}

for conflict in "${conflicts[@]}"; do
    if [[ -f "${LIB_PATH}/installed/${conflict}" ]]; then
        installed_conflicts+=("${conflict}")
    fi
done

if [[ ${#installed_conflicts} -gt 0 ]]; then
    echo "error: installation of ${name} would conflict with the following already installed packages: ${installed_conflicts[*]}" >&2
    exit 2
fi

for dependency in "${dependencies[@]}"; do
    targets+=("${dependency}")
done

redo-ifchange "${targets[@]}" "${name}.sync" "${name}.copy"

if [[ -f ${LIB_PATH}/installed/${name} ]]; then
    echo "upgrade_dates+=$(date -Iseconds)" >>"${LIB_PATH}/installed/${name}"
else
    echo -n "files=(" >"${LIB_PATH}/installed/${name}"
    find "${CACHE_PATH}/${name}" | while read -r cache_filename; do
        filename=${TARGET_PATH}${cache_filename#${CACHE_PATH}/${name}}
        echo -n "'${filename}' " >>"${LIB_PATH}/installed/${name}"
    done
    echo ")" >>"${LIB_PATH}/installed/${name}"
    echo "install_date=$(date -Iseconds)" >>"${LIB_PATH}/installed/${name}"
fi
